// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.7.4;

contract DummyUniswapRouter {

    event LiquidityAdded(address token, uint amountTokenDesired, uint amountTokenMin, uint amountETHMin, address to, uint deadline);

    constructor() public {

    }
//    function _addLiquidity(
//        address tokenA,
//        address tokenB,
//        uint amountADesired,
//        uint amountBDesired,
//        uint amountAMin,
//        uint amountBMin
//    ) private returns (uint amountA, uint amountB) {
//        // create the pair if it doesn't exist yet
//        if (IUniswapV2Factory(factory).getPair(tokenA, tokenB) == address(0)) {
//            IUniswapV2Factory(factory).createPair(tokenA, tokenB);
//        }
//        (uint reserveA, uint reserveB) = UniswapV2Library.getReserves(factory, tokenA, tokenB);
//        if (reserveA == 0 && reserveB == 0) {
//            (amountA, amountB) = (amountADesired, amountBDesired);
//        } else {
//            uint amountBOptimal = UniswapV2Library.quote(amountADesired, reserveA, reserveB);
//            if (amountBOptimal <= amountBDesired) {
//                require(amountBOptimal >= amountBMin, 'UniswapV2Router: INSUFFICIENT_B_AMOUNT');
//                (amountA, amountB) = (amountADesired, amountBOptimal);
//            } else {
//                uint amountAOptimal = UniswapV2Library.quote(amountBDesired, reserveB, reserveA);
//                assert(amountAOptimal <= amountADesired);
//                require(amountAOptimal >= amountAMin, 'UniswapV2Router: INSUFFICIENT_A_AMOUNT');
//                (amountA, amountB) = (amountAOptimal, amountBDesired);
//            }
//        }
//    }

    function addLiquidityETH(
        address token,
        uint amountTokenDesired,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    ) external payable returns (uint amountToken, uint amountETH, uint liquidity) {
//        (amountToken, amountETH) = _addLiquidity(
//            token,
//            WETH,
//            amountTokenDesired,
//            msg.value,
//            amountTokenMin,
//            amountETHMin
//        );
//        address pair = UniswapV2Library.pairFor(factory, token, WETH);
//        TransferHelper.safeTransferFrom(token, msg.sender, pair, amountToken);
//        IWETH(WETH).deposit{value: amountETH}();
//        assert(IWETH(WETH).transfer(pair, amountETH));
//        liquidity = IUniswapV2Pair(pair).mint(to);
//        if (msg.value > amountETH) TransferHelper.safeTransferETH(msg.sender, msg.value - amountETH); // refund dust eth, if any
        emit LiquidityAdded(token, amountTokenDesired, amountTokenMin,  amountETHMin, to, deadline);
        return (0, 0, 0);
    }


}
