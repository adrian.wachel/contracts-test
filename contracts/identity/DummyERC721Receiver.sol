// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.7.4;

contract DummyERC721Receiver {
    bytes4 private constant ERC721_RECEIVED = 0x150b7a02;

    constructor() public {

    }

    function onERC721Received(address operator, address from, uint256 tokenId, bytes calldata data) external returns (bytes4) {
        return ERC721_RECEIVED;
    }

}
