// SPDX-License-Identifier: GPL-3.0-only

pragma solidity ^0.7.4;

import "../SafeMathLib.sol";
import "../Token.sol";

contract BasicYield {

    using SafeMathLib for uint;

    // this represents a single recipient of token rewards on a fixed schedule that does not depend on deposit or burn rate
    // it specifies an id (key to a map below) an marker for the last time it was updated, a deposit (of LP tokens) and a
    // burn rate of those LP tokens per block, and finally, the owner of the slot, who will receive the rewards
    struct Slot {
        uint id;
        uint lastUpdatedBlock;
        uint deposit;
        address owner;
    }

    // privileged key that can change key parameters, will change to dao later
    address public management;

    // the token that the rewards are made in
    Token public rewardToken;

    // the liquidity provider (LP) token
    Token public stakeToken;

    // maximum number of slots, changeable by management key
    uint public maxStakers = 0;

    // current number of stakers
    uint public numStakers = 0;

    // minimum deposit allowable to claim a slot
    uint public minimumDeposit = 0;

    // maximum deposit allowable (used to limit risk)
    uint public maximumDeposit = 1000 ether;

    // total liquidity tokens staked
    uint public totalStaked = 0;

    // total rewards distributed
    uint public totalRewards = 0;

    // this actually is apy
    uint public rewardsPerTokenPerBlock = 0;

    // map of slot ids to slots
    mapping (uint => Slot) public slots;

    // map of addresses to amount staked
    mapping (address => uint) public totalStakedFor;

    // map of total rewards by address
    mapping (address => uint) public totalRewardsFor;

    event ManagementUpdated(address oldMgmt, address newMgmt);
    event MaxStakersUpdated(uint oldMaxStakers, uint newMaxStakers);
    event MinDepositUpdated(uint oldMinDeposit, uint newMinDeposit);
    event MaxDepositUpdated(uint oldMaxDeposit, uint newMaxDeposit);
    event RewardsUpdated(uint oldRewards, uint newRewards);
    event SlotChangedHands(uint slotId, uint deposit, address owner);

    modifier managementOnly() {
        require (msg.sender == management, 'Only management may call this');
        _;
    }

    constructor(address rewardTokenAddr, address stakeTokenAddr, address mgmt, uint mxStkrs, uint rewards) {
        rewardToken = Token(rewardTokenAddr);
        stakeToken = Token(stakeTokenAddr);
        management = mgmt;
        maxStakers = mxStkrs;
        rewardsPerTokenPerBlock = rewards;
    }

    // only management can reset management key
    function setManagement(address newMgmt) public managementOnly {
        address oldMgmt = management;
        management = newMgmt;
        emit ManagementUpdated(oldMgmt, newMgmt);
    }

    // change the number of slots, should be done with care
    function setMaxStakers(uint newMaxStakers) public managementOnly {
        uint oldMaxStakers = maxStakers;
        maxStakers = newMaxStakers;
        emit MaxStakersUpdated(oldMaxStakers, maxStakers);
    }

    // change the minimum deposit to acquire a slot
    function setMinDeposit(uint newMinDeposit) public managementOnly {
        uint oldMinDeposit = minimumDeposit;
        minimumDeposit = newMinDeposit;
        emit MinDepositUpdated(oldMinDeposit, newMinDeposit);
    }

    // change the maximum deposit
    function setMaxDeposit(uint newMaxDeposit) public managementOnly {
        uint oldMaxDeposit = maximumDeposit;
        maximumDeposit = newMaxDeposit;
        emit MaxDepositUpdated(oldMaxDeposit, newMaxDeposit);
    }

    // change the number of slots, should be done with care
    function setRewardsPerTokenPerBlock(uint newRewards) public managementOnly {
        uint oldRewards = rewardsPerTokenPerBlock;
        rewardsPerTokenPerBlock = newRewards;
        emit RewardsUpdated(oldRewards, newRewards);
    }

    // compute the undistributed rewards for a slot
    function getRewards(uint slotId) public view returns (uint) {
        Slot storage slot = slots[slotId];
        uint blocksDifference = block.number - slot.lastUpdatedBlock;
        uint rewards = rewardsPerTokenPerBlock.times(blocksDifference).times(slot.deposit) / stakeToken.decimals();

        return rewards;
    }

    // this must be idempotent, it syncs both the rewards and the deposit burn atomically, and updates lastUpdatedBlock
    function updateSlot(uint slotId) public {
        Slot storage slot = slots[slotId];

        uint rewards = getRewards(slotId);

        // update this first to make burn and reward zero in the case of re-entrance
        slot.lastUpdatedBlock = block.number;

        if (rewards > 0) {
            // bookkeeping
            totalRewards = totalRewards.plus(rewards);
            totalRewardsFor[slot.owner] = totalStakedFor[slot.owner].plus(rewards);

            rewardToken.transfer(slot.owner, rewards);
        }
    }

    // most important function for users, allows them to start receiving rewards
    function claimSlot(uint slotId, uint deposit) public {
        require(slotId > 0, 'Slot id must be positive');
        require(slotId <= maxStakers, 'Slot id out of range');
        require(deposit >= minimumDeposit, 'Deposit must meet or exceed minimum');
        require(deposit <= maximumDeposit, 'Deposit must not exceed maximum');

        Slot storage slot = slots[slotId];

        // count the stakers
        if (slot.owner == address(0)) {
            // assign id since this may be the first time
            slot.id = slotId;
            numStakers = numStakers.plus(1);
            slot.lastUpdatedBlock = block.number;
        } else {
            updateSlot(slotId);

            bool betterDeal = deposit > slot.deposit;
            require(betterDeal || slot.deposit == 0, 'You must outbid the current owner');

            // bookkeeping
            totalStaked = totalStaked.minus(slot.deposit);
            totalStakedFor[slot.owner] = totalStakedFor[slot.owner].minus(slot.deposit);

            // withdraw current owner
            withdrawFromSlotInternal(slotId);
        }

        // set new owner, burn rate
        slot.owner = msg.sender;
        slot.deposit = deposit;

        // bookkeeping
        totalStaked = totalStaked.plus(deposit);
        totalStakedFor[msg.sender] = totalStakedFor[msg.sender].plus(deposit);

        // transfer the tokens!
        stakeToken.transferFrom(msg.sender, address(this), deposit);

        emit SlotChangedHands(slotId, deposit, msg.sender);
    }

    // separates user from slot, if either voluntary or delinquent
    function withdrawFromSlot(uint slotId) public {
        Slot storage slot = slots[slotId];
        updateSlot(slotId);
        require(slot.owner == msg.sender || slot.deposit == 0, 'Only owner can call this unless user is delinquent');
        withdrawFromSlotInternal(slotId);

        // zero out owner and burn rate
        slot.owner = address(0);
        slot.lastUpdatedBlock = block.number;
        numStakers = numStakers.minus(1);
        emit SlotChangedHands(slotId, 0, address(0));
    }

    // internal function for withdrawing from a slot
    function withdrawFromSlotInternal(uint slotId) internal {
        Slot storage slot = slots[slotId];
        // if there's any deposit left,
        if (slot.deposit > 0) {
            uint deposit = slot.deposit;
            slot.deposit = 0;
            stakeToken.transfer(slot.owner, deposit);
        }
    }

}
