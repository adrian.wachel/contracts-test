#!/bin/bash

rm -rf geth-tmp
mkdir geth-tmp
cp genesis.json geth-tmp/
geth --datadir geth-tmp init genesis.json
geth --datadir geth-tmp account import --password <(true) <(echo 0000000000000000000000000000000000000000000000000000000000000001)
geth --datadir geth-tmp --dev --mine --nodiscover --networkid 333 --rpcaddr 127.0.0.1 --http --miner.gastarget 25000000000 \
    --unlock 0,1,2,3,4,5,6,7,8,9 --allow-insecure-unlock
