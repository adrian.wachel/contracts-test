const SafeMathLib = artifacts.require("SafeMathLib");
const Identity = artifacts.require("Identity");
const Token = artifacts.require("Token")
const Auction = artifacts.require('Auction')
const LiquidityMining = artifacts.require('LiquidityMining')
const MostBasicYield = artifacts.require('MostBasicYield')
const LPToken = artifacts.require('DummyLPToken')
const Trollbox = artifacts.require('Trollbox')
const Bridge = artifacts.require('Bridge')
const ERC20Handler = artifacts.require('ERC20Handler')
const TrollboxProxy = artifacts.require('TrollboxProxy')
const Vault = artifacts.require('Vault')
const UniformTimeVault = artifacts.require('UniformTimeVault')
const DummyUniswapRouter = artifacts.require('DummyUniswapRouter')
const DummyChainLinkAggregator = artifacts.require('DummyChainLinkAggregator')
const ReferralProgram = artifacts.require('ReferralProgram')
const ChainLinkOracle = artifacts.require('ChainLinkOracle2')
const numTokens = '1000000000'

let uniswapAddress = '', safeAddress = '', startBlock = 0, mgmt = ''

let tokenAddress
  , destinations = []
  , tokenAllocations = []
  , lockPeriods = []
  , vestingPeriodEnds = []
  , startBlocks = []

async function deployVault (deployer, network, accounts) {
    if (network === 'ropsten') {
        // FVT
        tokenAddress = '0x61dc5c017591ed096dd436fb2812f5954be8cd48'
        destinations = ['0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1']
        tokenAllocations = [web3.utils.toWei(numTokens)]
        lockPeriods = [4 * 60]
        vestingPeriodEnds = [4 * 60]
        startBlocks = [8926913 + (4 * 10)]
    } else if (network === 'development') {
        await deployer.deploy(SafeMathLib)
        token = await Token.deployed();
        tokenAddress = token.address;
        destinations = ['0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1', '0xFFcf8FDEE72ac11b5c542428B35EEF5769C409f0']
        tokenAllocations = [web3.utils.toWei('10000000'), web3.utils.toWei('10000000')]
        lockPeriods = [26, 29]
        vestingPeriodEnds = [25, 26]
        startBlocks = [10, 12]
    } else if (network === 'mainnet') {

    }
    await deployer.link(SafeMathLib, Vault)
    await deployer.deploy(Vault, tokenAddress, destinations, tokenAllocations, lockPeriods, vestingPeriodEnds, startBlocks);
}

async function getChainNow() {
    const tip = await web3.eth.getBlock('latest')
    return web3.utils.toBN(tip.timestamp);
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

async function deploySafeMath(deployer, network, accounts) {
  const ganacheGasLimit = '6721975';
  await deployer.deploy(
    SafeMathLib,
    network === 'development' ? {from: accounts[0], gasPrice: '1', gas: ganacheGasLimit}
                              : {from: accounts[0], gas: ganacheGasLimit})
}

async function deployIdentity(deployer, network, accounts) {
  if (network === 'development') {
    mgmt = accounts[0];
  } else if (network === 'ropsten') {
    mgmt = accounts[0];
  } else if (network === 'main') {
    mgmt = '0x288fE43139741F91a8Cbb6F4adD83811c794851b'
  }
  await deployer.link(SafeMathLib, Identity);
  await deployer.deploy(Identity, mgmt, {from: accounts[0]})
}

async function deployReferralProgram(deployer, network, accounts) {
  const identity = await Identity.deployed()
  await deployer.deploy(ReferralProgram, identity.address, {from: accounts[0]})
}

async function connectContracts() {
  const identity = await Identity.deployed()
  const token = await Token.deployed()
  const trollBox = await Trollbox.deployed()
  await identity.setToken(token.address)
  await trollBox.setToken(token.address)
}

async function deployAuction (deployer, network, accounts) {
  console.log('network', network)
  console.log('accounts', accounts)
  const tokensForSale = web3.utils.toWei('20000000')
  const initialPrice = web3.utils.toWei('0.000025') // approximately 0.01 USD
  const initialTokens = web3.utils.toWei('500000') // half a million tokens
  const initialDecay = web3.utils.toWei('0.00000001') // 1% of price floor, 1/2500 of starting price
  const minimumPrice = web3.utils.toWei('0.000001') // 4% of starting price

  if (network === 'development') {
    mgmt = accounts[0]
    await deployer.deploy(DummyUniswapRouter)
    const uniswap = await DummyUniswapRouter.deployed()

    startBlock = 0
    tokenAddress = (await Token.deployed()).address
    uniswapAddress = uniswap.address
    safeAddress = accounts[1]
  } else if (network === 'ropsten') {
    mgmt = '0x288fE43139741F91a8Cbb6F4adD83811c794851b'
    startBlock = 8997620
    tokenAddress = '0xF7eF90B602F1332d0c12cd8Da6cd25130c768929'
    uniswapAddress = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
    safeAddress = '0x96FFB474BCFE44235082c07C7f3BE07cb9fbC08f'
  } else if (network === 'main') {
    mgmt = ''
    tokenAddress = ''
    startBlock = 0
    uniswapAddress = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
    safeAddress = ''
  }
  await deployer.link(SafeMathLib, Auction)
  await deployer.deploy(
    Auction,
    mgmt,
    tokenAddress,
    uniswapAddress,
    startBlock,
    tokensForSale,
    initialPrice,
    initialTokens,
    initialDecay,
    minimumPrice,
    safeAddress
  )
  const auction = await Auction.deployed()

  if (network !== 'main') {
    const fvt = await Token.at(tokenAddress)
    await fvt.transfer(auction.address, tokensForSale)
  }
}

async function deployToken(deployer, network, accounts) {
    let bank;
    if (network === 'ropsten') {
        bank = '0x288fE43139741F91a8Cbb6F4adD83811c794851b'
    } else if (network === 'development') {
        await deployer.deploy(SafeMathLib);
        bank = accounts[0]
    } else if (network === 'main') {

    }
    await deployer.link(SafeMathLib, Token);
    await deployer.deploy(Token, bank);
}

async function deployTrollbox (deployer, network, accounts) {
  if (network === 'development') {
    mgmt = accounts[0]
    rankManager = accounts[1]
  } else if (network === 'ropsten') {
    mgmt = accounts[0]
    rankManager = accounts[0]
  } else if (network === 'main') {
    mgmt = '0x288fE43139741F91a8Cbb6F4adD83811c794851b'
    rankManager = mgmt
  }
  const i = await Identity.deployed()
  identityAddress = i.address
  await deployer.link(SafeMathLib, Trollbox)
  await deployer.deploy(Trollbox, mgmt, rankManager, identityAddress, {from: accounts[0]})
  const trollbox = await Trollbox.deployed()
  await deployer.deploy(TrollboxProxy, trollbox.address)
}

async function deployLiquidityMining(deployer, network, accounts) {
  mgmt = accounts[0]
  const pulseLengthBlocks = 10000
  const pulseAmplitude = web3.utils.toWei('1000')
  const maxStakers = 10
  await deployer.link(SafeMathLib, LPToken);
  await deployer.deploy(LPToken, mgmt)
  const baseToken = await Token.deployed()
  const lpToken = await LPToken.deployed()
  await deployer.link(SafeMathLib, LiquidityMining)
  await deployer.deploy(LiquidityMining, baseToken.address, lpToken.address, mgmt, pulseLengthBlocks, pulseAmplitude, maxStakers)

  const decimals = web3.utils.toBN(web3.utils.toWei('1'))
  const maxDeposit = await web3.utils.toWei('1000000')
  const rewards = ['10000000', '20000000']
  const programLengthDays = 30
  const programLengthSeconds = web3.utils.toBN(programLengthDays * 60 * 60 * 24)
  await deployer.deploy(Token, mgmt)
  const token1 = await Token.deployed()
  await deployer.deploy(Token, mgmt)
  const token2 = await Token.deployed()
  const startTime = 0
  await deployer.link(SafeMathLib, MostBasicYield);
  await deployer.deploy(MostBasicYield, startTime, maxDeposit, rewards, programLengthDays, token1.address, [token1.address, token2.address], mgmt)
  const basicYield = await MostBasicYield.deployed()
  const maxDepositTime = web3.utils.toBN(maxDeposit).mul(programLengthSeconds)
  const maxReward1 = maxDepositTime.mul(web3.utils.toBN(rewards[0])).div(decimals)
  const maxReward2 = maxDepositTime.mul(web3.utils.toBN(rewards[1])).div(decimals)
  console.log(maxDepositTime.toString(), maxReward1.toString(), maxReward2.toString())
  await token1.transfer(basicYield.address, maxReward1, {from: accounts[0]})
  await token2.transfer(basicYield.address, maxReward2, {from: accounts[0]})
}

async function fundContracts (deployer, network, accounts) {
  mgmt = accounts[0]
  const token = await Token.deployed()
  const auction = await Auction.deployed()
  const vault = await Vault.deployed()
  const trollBox = await Trollbox.deployed()
  const liquidityMining = await LiquidityMining.deployed()
  await token.transfer(auction.address, web3.utils.toWei('20000000'), {from: mgmt})
  await token.transfer(vault.address, web3.utils.toWei('20000000'), {from: mgmt})
  await token.transfer(trollBox.address, web3.utils.toWei('20000000'), {from: mgmt})
  await token.transfer(liquidityMining.address, web3.utils.toWei('20000000'), {from: mgmt})
}

async function deployChainLink (deployer, network, accounts) {
  mgmt = accounts[0]
  const trollbox = await Trollbox.deployed()
  const token = await Token.deployed()
  const defaultTickerSymbols = ['LINK', 'WBTC', 'DAI', 'UNI', 'MKR', 'COMP', 'UMA', 'YFI', 'SNX', 'REN'].map(x => web3.utils.asciiToHex(x));
  const initialPrices = ['1',"2",'3','4','5','6','7','8','9','10'].map(x => web3.utils.toWei(x))
  console.log({initialPrices, defaultTickerSymbols})
  await deployer.deploy(ChainLinkOracle, mgmt, trollbox.address, token.address, 1, 1, defaultTickerSymbols, initialPrices)
  const oracle = await ChainLinkOracle.deployed()
  const aggs = {}
  for (var i = 0; i < defaultTickerSymbols.length; i++) {
    const price = web3.utils.toWei(Math.floor(Math.random() * 100).toString())
    const rcpt = await deployer.deploy(DummyChainLinkAggregator, price)
    const symbol = defaultTickerSymbols[i]
    aggs[web3.utils.hexToString(symbol)] = rcpt.contract._address
    await oracle.addFeed(symbol, rcpt.contract._address)
  }
  console.log(aggs)
}

async function deployBridge (deployer, network, accounts) {
  const chainId = 0
  const initialRelayers = ['0x2E70f28C39c8d1f1b737cD37e1EC76b72bD87cf6']
  const initialRelayerThreshold = 1
  const fee = 0
  const expiry = 100

  await deployer.deploy(Bridge, chainId, initialRelayers, initialRelayerThreshold, fee, expiry, {from: accounts[0]})
  const bridge = await Bridge.deployed()
  const token = await Token.deployed()
  const resourceId = web3.utils.sha3('ETH/BNB')

  await deployer.deploy(ERC20Handler, bridge.address, [], [], [])
  const handler = await ERC20Handler.deployed()

  // this connects the bridge to the handler and the token
  await bridge.adminSetResource(handler.address, resourceId, token.address, {from: accounts[0]})

  const chainId2 = 1
  const initialRelayers2 = initialRelayers
  const initialRelayerThreshold2 = 1
  const fee2 = 0
  const expiry2 = 100
  await deployer.deploy(Bridge, chainId2, initialRelayers2, initialRelayerThreshold2, fee2, expiry2, {from: accounts[0]})
  const bridge2 = await Bridge.deployed()

  await deployer.deploy(ERC20Handler, bridge2.address, [], [], [])
  const handler2 = await ERC20Handler.deployed()

  // this connects the bridge to the handler and the token
  await bridge2.adminSetResource(handler2.address, resourceId, token.address, {from: accounts[0]})
  await bridge2.adminSetBurnable(handler2.address, token.address, {from: accounts[0], gasPrice: '10000000000'})
}

async function deployVaults(deployer, network, accounts) {
  await deployer.link(SafeMathLib, UniformTimeVault)
  const token = await Token.deployed()
  const numTranches = 30;
  const destinations = [];
  const tokenAllocations = [];
  const chainNow = await getChainNow();
  startTime = chainNow.sub(web3.utils.toBN(getRandomInt(100)));
  endTime = startTime.add(web3.utils.toBN(getRandomInt(1000)));

  for (var i = 0; i < numTranches; i++) {
      destinations.push(accounts[i % 10]);

      const allocation = web3.utils.toWei(web3.utils.toBN((getRandomInt(10) + 1) * 100))
      tokenAllocations.push(allocation);
  }

  await deployer.deploy(UniformTimeVault,
      token.address,
      destinations,
      tokenAllocations,
      endTime,
      startTime
  );
  const vault = await UniformTimeVault.deployed()

  await token.transfer(vault.address, web3.utils.toWei('1000000'), {from: accounts[0]});

}

module.exports = function(deployer, network, accounts) {
  deployer.then(async () => {
    await deploySafeMath(deployer, network, accounts)
    await deployIdentity(deployer, network, accounts);
    await deployReferralProgram(deployer, network, accounts)
    await deployToken(deployer, network, accounts);
    await deployTrollbox(deployer, network, accounts);
    await connectContracts()
    await deployVault(deployer, network, accounts)
    await deployVaults(deployer, network, accounts)
    await deployAuction(deployer, network, accounts)
    await deployLiquidityMining(deployer, network, accounts)
    await fundContracts(deployer, network, accounts)
    await deployChainLink(deployer, network, accounts)
    await deployBridge(deployer, network, accounts)
  });
};
