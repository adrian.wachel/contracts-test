/* global artifacts, web3 */

const Trollbox = artifacts.require('Trollbox')
const TrollboxProxy = artifacts.require('TrollboxProxy')
const LiquidityMining = artifacts.require('LiquidityMining')
const MostBasicYield = artifacts.require('MostBasicYield')
const ChainLinkOracle = artifacts.require('ChainLinkOracle2')
const LPToken = artifacts.require('DummyLPToken')
const Token = artifacts.require('Token')
const Identity = artifacts.require('Identity')
const tournamentHash = '0x2bffe98db7ae36ac27dd176b5b4f1ddf1d319d2084570230ed75dfdca89030ea'
const voteHash = tournamentHash
const startTime = 0
const tournamentId = 1
const roundLengthSeconds = 30
const tokenRoundBonus = web3.utils.toWei('100')
const tokenListENS = web3.utils.stringToHex('defi.cmc.eth')
const minRank = 0
const voiceUBI = 100

async function mineOneBlock () {
  await web3.currentProvider.send({
    jsonrpc: '2.0',
    method: 'evm_mine',
    id: new Date().getTime()
  }, () => {})
}

async function increaseTime(bySeconds) {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_increaseTime',
        params: [bySeconds],
        id: new Date().getTime(),
    }, (err, result) => {
      if (err) { console.error(err) }

    });
    await mineOneBlock();
}


async function generateEventsTrollbox (deployer, network, accounts) {
  if (network === 'development') {
    const trollbox = await Trollbox.deployed()
    const proxy = await TrollboxProxy.deployed()
    const oracle = await ChainLinkOracle.deployed()
    const challengePeriod = 10
    await trollbox.createTournament(tournamentHash, startTime, roundLengthSeconds, tokenRoundBonus, tokenListENS, oracle.address, minRank, voiceUBI)
    await oracle.setChallengePeriod(challengePeriod, {from: accounts[0]})

    const identity = await Identity.deployed()
    await identity.createIdentityFor(accounts[0], { from: accounts[0] })
    await identity.createIdentityFor(accounts[1], { from: accounts[0] })
    await identity.createIdentityFor(accounts[2], { from: accounts[0] })
    await identity.createIdentityFor(accounts[3], { from: accounts[0] })
    await identity.createIdentityFor(accounts[4], { from: accounts[0] })
    const roundId = await trollbox.getCurrentRoundId(1)
    const t = await trollbox.tournaments(1)
    await trollbox.vote(2, tournamentId, [ 1, 2, 3 ], [ 1, 2, 3 ], voteHash, 0, { from: accounts[1] })
    await trollbox.vote(3, tournamentId, [ 1, 2, 3 ], [ 1, 2, 3 ], voteHash, 0, { from: accounts[2] })
    await trollbox.vote(4, tournamentId, [ 1, 2, 3 ], [ 1, 2, 3 ], voteHash, 0, { from: accounts[3] })
    await trollbox.vote(5, tournamentId, [ 1, 2 ], [ 4, 8 ], voteHash, 0, { from: accounts[4] })
    await increaseTime(roundLengthSeconds + 10)
    await trollbox.vote(2, tournamentId, [ 1, 2, 3 ], [ 3, 2, 3 ], voteHash, 0, { from: accounts[1] })
    await trollbox.vote(3, tournamentId, [ 1, 2, 3 ], [ 3, 2, 3 ], voteHash, 0, { from: accounts[2] })
    await trollbox.vote(4, tournamentId, [ 1, 2, 3 ], [ 3, 2, 3 ], voteHash, 0, { from: accounts[3] })
    await trollbox.vote(5, tournamentId, [ 1, 6 ], [ 4, 8 ], voteHash, 0, { from: accounts[4] })
    await increaseTime(roundLengthSeconds + 10)

    await increaseTime(roundLengthSeconds + 10)

//    await trollbox.resolveRound(tournamentId, 1, 2, { from: accounts[0] })
    const rcpt = await oracle.proposeWinner({ from: accounts[0] })
    console.log(rcpt)
    await increaseTime(challengePeriod + 1)
    await oracle.confirmWinnerUnchallenged({ from: accounts[0] })

    for (let i = 0; i < 15; i++) {
      const roundId = 2 + i
      await trollbox.vote(5, tournamentId, [ 1, 6 ], [ 4, 8 ], voteHash, 0, { from: accounts[4] })
      await increaseTime(roundLengthSeconds + 10)
      await oracle.proposeWinner({ from: accounts[0] })
      await increaseTime(challengePeriod + 1)
      await oracle.confirmWinnerUnchallenged({ from: accounts[0] })
    }
  } else if (network === 'ropsten') {
    const trollbox = await Trollbox.deployed()
    await trollbox.createTournament(tournamentHash, startTime, roundLengthSeconds, tokenRoundBonus, tokenListENS, accounts[0], minRank, voiceUBI)
  }
}

async function generateEventsYield (deployer, network, accounts) {
  const lpToken = await LPToken.deployed()
  const liquidityMining = await LiquidityMining.deployed()

  async function claimSlot(slotId, burnRate, deposit, fromAddr) {
    await lpToken.approve(liquidityMining.address, web3.utils.toWei(deposit), {from: fromAddr})
    await liquidityMining.claimSlot(slotId, web3.utils.toWei(burnRate), web3.utils.toWei(deposit), {from: fromAddr})
  }

  await lpToken.transfer(accounts[1], web3.utils.toWei('10000000'), {from: accounts[0]})
  await lpToken.transfer(accounts[2], web3.utils.toWei('10000000'), {from: accounts[0]})
  await lpToken.transfer(accounts[3], web3.utils.toWei('10000000'), {from: accounts[0]})
  await lpToken.transfer(accounts[4], web3.utils.toWei('10000000'), {from: accounts[0]})
  await lpToken.transfer(accounts[5], web3.utils.toWei('10000000'), {from: accounts[0]})
  await lpToken.transfer(accounts[6], web3.utils.toWei('10000000'), {from: accounts[0]})

  await claimSlot(1, '1', '10', accounts[1])
  await claimSlot(2, '2', '20', accounts[2])
  await claimSlot(3, '3', '100', accounts[3])
  await claimSlot(4, '5', '50', accounts[4])
  await claimSlot(5, '10', '1000', accounts[5])
  await claimSlot(6, '1', '10', accounts[6])

  const mostBasicYield = await MostBasicYield.deployed()
  const depositTokenAddress =  await mostBasicYield.depositToken()
  const depositToken = await Token.at(depositTokenAddress)
  const maxDeposit = await mostBasicYield.maximumDeposit()
//  const rewards1 = await mostBasicYield.tokensPerSecondPerToken(0)
//  const rewardToken1 = await mostBasicYield.rewardTokens(0)
//  const rewards2 = await mostBasicYield.tokensPerSecondPerToken(1)
//  const rewardToken2 = await mostBasicYield.rewardTokens(1)
  let deposit = web3.utils.toBN(web3.utils.toWei('10'))
  let numIters = 30;
  for (let i = 0;  i < 10; i++) {
    await depositToken.approve(mostBasicYield.address, web3.utils.toBN(web3.utils.toWei('9999999999')), { from: accounts[i], nonce: (await web3.eth.getTransactionCount(accounts[i])) })
  }

  for (let i = 1; i < 10; i++) {
    await depositToken.transfer(accounts[i], web3.utils.toWei('1001'), { from: accounts[0] })
  }

  for (let i = 0; i < numIters; i++) {
    let user = accounts[i % 10]
    await mostBasicYield.deposit(deposit, { from: user })
  }

  for (let i = 1; i <= numIters / 2; i++) {
    const receipt = await mostBasicYield.receipts(i)
    const owner = receipt.owner
    const rcpt2 = await mostBasicYield.withdraw(i, {from: owner})
  }

}

module.exports = (deployer, network, accounts) => {
  deployer.then(async () => {
    await generateEventsTrollbox(deployer, network, accounts)
    await generateEventsYield(deployer, network, accounts)
  })
}
