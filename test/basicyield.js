/* global artifacts, web3, afterEach, beforeEach, it, contract */

const Web3 = require('web3')
const web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545/', {
  clientConfig: { keepAlive: true, timeout: 2000*1000 },
}))
const bN = web3.utils.toBN
const l = console.log
const MostBasicYield = artifacts.require('MostBasicYield')
const Token = artifacts.require('Token')
const SafeMathLib = artifacts.require('SafeMathLib')

const {
  BN, // Big Number support
  constants, // Common constants, like the zero address and largest integers
  expectEvent, // Assertions for emitted events
  expectRevert // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers')

const { ZERO_BYTES32, ZERO_ADDRESS } = constants // empty hash

const chai = require('chai')
const bnChai = require('bn-chai')
chai.use(bnChai(BN))
chai.config.includeStack = true
const expect = chai.expect

async function mineOneBlock () {
  await web3.currentProvider.send({
    jsonrpc: '2.0',
    method: 'evm_mine',
    id: new Date().getTime()
  }, () => {})
}

async function mineBlocks (numBlocks) {
  for (var i = 0; i < numBlocks; i++) {
    await mineOneBlock()
  }
}

async function getChainHeight () {
  const tip = await web3.eth.getBlock('latest')
  return new BN(tip.number)
}

async function getChainNow () {
  const tip = await web3.eth.getBlock('latest')
  return new BN(tip.timestamp)
}

async function increaseTime (bySeconds) {
  await web3.currentProvider.send({
    jsonrpc: '2.0',
    method: 'evm_increaseTime',
    params: [ bySeconds ],
    id: new Date().getTime()
  }, (err, result) => {
    if (err) { console.error(err) }
  })
  await mineOneBlock()
}


async function getTxTime(rcpt) {
  const blockNumber = rcpt.receipt.blockNumber
  const block = await web3.eth.getBlock(blockNumber)
  return block.timestamp
}

contract('Most Basic Yield', function (accounts) {
  const state = {}
  const decimals = new BN('1000000000000000000')

  beforeEach(async () => {
    const safeMath = await SafeMathLib.new({from: accounts[2], nonce: (await web3.eth.getTransactionCount(accounts[2]))})
    await Token.link('SafeMathLib', safeMath.address)
    await MostBasicYield.link('SafeMathLib', safeMath.address)
    state.mgmt = accounts[0]
    state.initialBalance = new BN(1e9)
    state.depositToken = await Token.new(state.mgmt, {from: accounts[2], nonce: (await web3.eth.getTransactionCount(accounts[2]))})
    state.rewardToken1 = await Token.new(state.mgmt, {from: accounts[2], nonce: (await web3.eth.getTransactionCount(accounts[2]))})
    state.rewardToken2 = await Token.new(state.mgmt, {from: accounts[2], nonce: (await web3.eth.getTransactionCount(accounts[2]))})
    state.rewardTokens = [state.rewardToken1.address, state.rewardToken2.address],
    state.reward1 = new BN('10')
    state.reward2 = new BN('20')
    state.rewards = [state.reward1, state.reward2]
    state.programLengthDays = 1
    state.programLengthSeconds = 60 * 60 * 24 * state.programLengthDays
    state.maxDeposits = web3.utils.toWei('1000')
    state.totalRewards1 = (new BN(state.programLengthSeconds)).mul(state.reward1).mul(new BN(state.maxDeposits).div(decimals))
    state.totalRewards2 = (new BN(state.programLengthSeconds)).mul(state.reward2).mul(new BN(state.maxDeposits).div(decimals))
    state.startTime = 0
    state.yield = await MostBasicYield.new(
      state.startTime,
      state.maxDeposits,
      [state.reward1, state.reward2],
      state.programLengthDays,
      state.depositToken.address,
      state.rewardTokens,
      state.mgmt,
      {from: accounts[2], nonce: (await web3.eth.getTransactionCount(accounts[2]))}
    )
    state.chainHeightStart = await getChainHeight()
    state.gasLimit = 170000
    state.safeAddress = accounts[9]

    await state.rewardToken1.transfer(state.yield.address, state.totalRewards1, { from: state.mgmt, nonce: (await web3.eth.getTransactionCount(state.mgmt))})
    await state.rewardToken2.transfer(state.yield.address, state.totalRewards2, { from: state.mgmt, nonce: (await web3.eth.getTransactionCount(state.mgmt))})
    for (let i = 1; i < 10; i++) {
      await state.depositToken.transfer(accounts[i], web3.utils.toWei('1001'), { from: state.mgmt })
    }
  })

  afterEach(() => {})

  it('initializes values correctly', async function () {
      expect(await state.yield.management()).to.be.a('string').that.equals(state.mgmt)
      expect(await state.yield.depositToken()).to.be.a('string').that.equals(state.depositToken.address)
      expect(await state.yield.rewardTokens(0)).to.be.a('string').that.equals(state.rewardTokens[0])
      expect(await state.yield.rewardTokens(1)).to.be.a('string').that.equals(state.rewardTokens[1])
      expect(await state.yield.tokensPerSecondPerToken(0)).to.eq.BN(state.rewards[0])
      expect(await state.yield.tokensPerSecondPerToken(1)).to.eq.BN(state.rewards[1])
      expect(await state.yield.maximumDeposit()).to.eq.BN(state.maxDeposits)
      expect(await state.rewardToken1.balanceOf(state.yield.address)).to.eq.BN(state.totalRewards1)
      expect(await state.rewardToken2.balanceOf(state.yield.address)).to.eq.BN(state.totalRewards2)
  })

  it('allows depositing and withdrawing', async function  () {
      const receiptBefore = await state.yield.receipts(1)
      const deposit = web3.utils.toWei('100')
      const depositor = accounts[1]
      const notDepositor = accounts[2]
      const depositBalanceBefore = await state.depositToken.balanceOf(depositor)
      const reward1BalanceBefore = await state.rewardToken1.balanceOf(depositor)
      const reward2BalanceBefore = await state.rewardToken2.balanceOf(depositor)
      await state.depositToken.approve(state.yield.address, deposit, {from: depositor})
      const rcpt = await state.yield.deposit(deposit, { from: depositor })
      expectEvent(rcpt, 'DepositOccurred', {id: '1', owner: depositor })

      const depositBalanceAfter = await state.depositToken.balanceOf(depositor)
      const reward1BalanceAfter = await state.rewardToken1.balanceOf(depositor)
      const reward2BalanceAfter = await state.rewardToken2.balanceOf(depositor)

      const receiptAfter = await state.yield.receipts(1)
      const timeBefore = await getTxTime(rcpt)
      const rewards = await state.yield.getRewards(1)
      const rcpt2 = await state.yield.withdraw(1, { from: depositor })
      expectEvent(rcpt2, 'WithdrawalOccurred', {id: '1', owner: depositor })

      const depositBalanceAfter2 = await state.depositToken.balanceOf(depositor)
      const reward1BalanceAfter2 = await state.rewardToken1.balanceOf(depositor)
      const reward2BalanceAfter2 = await state.rewardToken2.balanceOf(depositor)

      const timeAfter = await getTxTime(rcpt2)
      const expectedRewards1 = (new BN(deposit)).muln(timeAfter - timeBefore).mul(state.reward1).div(decimals)
      const expectedRewards2 = (new BN(deposit)).muln(timeAfter - timeBefore).mul(state.reward2).div(decimals)
      const receiptAfter2 = await state.yield.receipts(1)

      expect(receiptBefore.id).to.eq.BN(0)
      expect(receiptBefore.owner).to.be.a('string').that.equals(ZERO_ADDRESS)

      expect(receiptAfter.id).to.eq.BN(1)
      expect(receiptAfter.owner).to.be.a('string').that.equals(depositor)

      expect(receiptAfter2.id).to.eq.BN(1)
      expect(receiptAfter2.owner).to.be.a('string').that.equals(depositor)

      expect(depositBalanceBefore).to.eq.BN(depositBalanceAfter2)
      expect(depositBalanceBefore).to.eq.BN(depositBalanceAfter.add(new BN(deposit)))
      expect(reward1BalanceBefore).to.eq.BN(0)
      expect(reward1BalanceAfter).to.eq.BN(0)
      expect(reward1BalanceAfter2).to.eq.BN(expectedRewards1)
      expect(reward2BalanceBefore).to.eq.BN(0)
      expect(reward2BalanceAfter).to.eq.BN(0)
      expect(reward2BalanceAfter2).to.eq.BN(expectedRewards2)

      await state.depositToken.approve(state.yield.address, deposit, {from: depositor})
      await state.yield.deposit(deposit, { from: depositor })
      await expectRevert(state.yield.withdraw(2, { from: notDepositor }), 'Can only withdraw your own deposit')

  })

  it('allows anyone to force withdrawal after end', async () => {
      const deposit = web3.utils.toWei('100')
      const depositor = accounts[1]
      await state.depositToken.approve(state.yield.address, deposit, {from: depositor})
      const rcpt = await state.yield.deposit(deposit, { from: depositor })
      await expectRevert(state.yield.withdraw(1, {from: state.mgmt}), 'Can only withdraw your own deposit')
      await increaseTime(state.programLengthSeconds)
      await state.yield.withdraw(1, {from: state.mgmt, nonce: (await web3.eth.getTransactionCount(state.mgmt))})
  })

  it('allows anyone to withdraw excess rewards to management after end', async () => {
      const deposit = web3.utils.toWei('100')
      const depositor = accounts[1]
      await state.depositToken.approve(state.yield.address, deposit, {from: depositor})
      const rcpt = await state.yield.deposit(deposit, { from: depositor })
      await expectRevert(state.yield.withdrawExcessRewards({from: state.mgmt}), 'Cannot withdraw until all deposits are withdrawn')
      await state.yield.withdraw(1, {from: depositor})
      await expectRevert(state.yield.withdrawExcessRewards({from: accounts[2], nonce: (await web3.eth.getTransactionCount(accounts[2]))}), 'Contract must reach maturity')
      await increaseTime(state.programLengthSeconds)
      await state.yield.withdrawExcessRewards({from: accounts[3], nonce: (await web3.eth.getTransactionCount(accounts[3]))})
  })

  it('does not allow depositing more than maximum', async () => {
      const deposit = web3.utils.toWei('1001')
      const depositor = accounts[1]
      const depositBalanceBefore = await state.depositToken.balanceOf(depositor)
      await state.depositToken.approve(state.yield.address, deposit, {from: depositor})
      const rcpt = await state.yield.deposit(deposit, { from: depositor })
      expectEvent(rcpt, 'DepositOccurred', {id: '1', owner: depositor })
      const depositBalanceAfter = await state.depositToken.balanceOf(depositor)
      expect(depositBalanceAfter).to.eq.BN(depositBalanceBefore.sub(new BN(state.maxDeposits)))
      await expectRevert(state.yield.deposit(web3.utils.toWei('1'), {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))}), 'Maximum deposit already reached')
  })

  it('does not allow depositing before startTime', async () => {
      const startTime = (await getChainNow()).add(web3.utils.toBN('111'))
      const yield = await MostBasicYield.new(
        startTime,
        state.maxDeposits,
        [state.reward1, state.reward2],
        state.programLengthDays,
        state.depositToken.address,
        state.rewardTokens,
        state.mgmt,
        {from: accounts[2], nonce: (await web3.eth.getTransactionCount(accounts[2]))}
      )
      const deposit = web3.utils.toWei('1001')
      const depositor = accounts[1]
      await state.depositToken.approve(yield.address, deposit, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))})
      await expectRevert(yield.deposit(deposit, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))}), 'Cannot deposit before pool start')
      await increaseTime(112)
      const rcpt = await yield.deposit(deposit, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))})
      expectEvent(rcpt, 'DepositOccurred', { id: '1', owner: depositor})
  })

  it('does not allow depositing after endTime', async () => {
      const now = await getChainNow()
      const endTime = await state.yield.endTime()
      const diff = endTime.sub(now).addn(1)
      await increaseTime(diff.toNumber())
      const deposit = web3.utils.toWei('1001')
      const depositor = accounts[1]
      await expectRevert(state.yield.deposit(deposit, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))}), 'Cannot deposit after pool ends')
  })

  it('rewards do not accumulate after endTime', async () => {
      const deposit = web3.utils.toWei('1001')
      const depositor = accounts[1]
      await increaseTime(10)
      await state.depositToken.approve(state.yield.address, deposit, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))})
      await state.yield.deposit(deposit, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))})

      const now = await getChainNow()
      const endTime = await state.yield.endTime()
      const diff = endTime.sub(now).addn(1)
      await increaseTime(diff.toNumber())

      const rewards1 = await state.yield.getRewards(1)
      await increaseTime(diff.toNumber())
      await mineOneBlock()
      const rewards2 = await state.yield.getRewards(1)
      expect(rewards2[0]).to.eq.BN(rewards1[0])
      expect(rewards2[1]).to.eq.BN(rewards1[1])

      const balance1Before = await state.rewardToken1.balanceOf(depositor)
      const balance2Before = await state.rewardToken2.balanceOf(depositor)
      const rcpt = await state.yield.withdraw(1, {from: depositor})
      expectEvent(rcpt, 'WithdrawalOccurred', {id: '1', owner: depositor})
      const balance1After = await state.rewardToken1.balanceOf(depositor)
      const balance2After = await state.rewardToken2.balanceOf(depositor)
      const expectedBalance1After = balance1Before.add(rewards1[0])
      const expectedBalance2After = balance2Before.add(rewards1[1])
      expect(balance1After).to.eq.BN(expectedBalance1After)
      expect(balance2After).to.eq.BN(expectedBalance2After)
  })

  it('cannot redeem receipts more than once', async () => {
      const deposit = web3.utils.toWei('1001')
      const depositor = accounts[1]
      await increaseTime(10)
      await state.depositToken.approve(state.yield.address, deposit, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))})
      await state.yield.deposit(deposit, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))})
      const rcpt = await state.yield.withdraw(1, {from: depositor})
      await expectRevert(state.yield.withdraw(1, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))}), 'Can only withdraw once per receipt')
  })

  it('cannot redeem undeposited receipts', async () => {
      const deposit = web3.utils.toWei('1001')
      const depositor = accounts[1]
      await increaseTime(10)
      await expectRevert(state.yield.withdraw(1, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))}), 'Can only withdraw real receipts')

      const now = await getChainNow()
      const endTime = await state.yield.endTime()
      const diff = endTime.sub(now).addn(1)
      await increaseTime(diff.toNumber())
      await expectRevert(state.yield.withdraw(1, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))}), 'Can only withdraw real receipts')
      await expectRevert(state.yield.withdraw(2, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))}), 'Can only withdraw real receipts')
      await expectRevert(state.yield.withdraw(3, {from: depositor, nonce: (await web3.eth.getTransactionCount(depositor))}), 'Can only withdraw real receipts')
  })

  it('allows random staking', async () => {
    let deposit = new BN(web3.utils.toWei('10'))
    let numIters = 100;
    for (let i = 0;  i < 10; i++) {
      await state.depositToken.approve(state.yield.address, new BN(web3.utils.toWei('9999999999')), { from: accounts[i], nonce: (await web3.eth.getTransactionCount(accounts[i])) })
    }

    for (let i = 0; i < numIters; i++) {
      let user = accounts[i % 10]
      const rcpt = await state.yield.deposit(deposit, { from: user })
      expectEvent(rcpt, 'DepositOccurred', {id: (i+1).toString(), owner: user})
    }
    for (let i = 1; i <= numIters; i++) {
      const receipt = await state.yield.receipts(i)
      const owner = receipt.owner
      const rcpt2 = await state.yield.withdraw(i, {from: owner})
      expectEvent(rcpt2, 'WithdrawalOccurred', {id: i.toString(), owner: owner})
    }
  })

})
