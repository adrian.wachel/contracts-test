'use strict';

const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545/', {
  clientConfig: { keepAlive: true, timeout: 2000*1000 },
}));
const bN = web3.utils.toBN;
const l = console.log;
const DummyChainLinkAggregator = artifacts.require('DummyChainLinkAggregator')
const DummyChainLinkAggregatorETH = artifacts.require('DummyChainLinkAggregatorETH')
    , SafeMathLib = artifacts.require('SafeMathLib')
    , Trollbox = artifacts.require('Trollbox')
    , Token = artifacts.require('Token')
    , ChainLinkOracle = artifacts.require('ChainLinkOracle2')
    , Identity = artifacts.require('Identity')


const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');

const { ZERO_BYTES32, ZERO_ADDRESS } = constants; // empty hash
const ONE_BYTES32 = '0x0000000000000000000000000000000000000000000000000000000000000001'

const chai = require('chai');
const bnChai = require('bn-chai');
chai.use(bnChai(BN));
chai.config.includeStack = true;
const expect = chai.expect;

async function mineOneBlock() {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: new Date().getTime(),
    }, () => {});
}

async function mineBlocks(numBlocks) {
    for (var i = 0; i < numBlocks; i++) {
        await mineOneBlock();
    }
}

async function getChainHeight () {
  const tip = await web3.eth.getBlock('latest')
  return new BN(tip.number)
}

async function getChainNow () {
  const tip = await web3.eth.getBlock('latest')
  return new BN(tip.timestamp)
}

async function increaseTime (bySeconds) {
  await web3.currentProvider.send({
    jsonrpc: '2.0',
    method: 'evm_increaseTime',
    params: [ bySeconds ],
    id: new Date().getTime()
  }, (err, result) => {
    if (err) { console.error(err) }
  })
  await mineOneBlock()
}

contract('Chainlink2', function (accounts) {
    const state = {};
    const decimals = new BN('1000000000000000000')

    async function createTournament () {
      const args = [
        state.defaultHash,
        state.defaultStartTime,
        state.defaultRoundLength,
        state.defaultBonus,
        state.defaultENS,
        state.oracle.address,
        state.defaultMinRank,
        state.defaultUBI
      ]
      await state.trollbox.createTournament(...args, { from: state.mgmt })
    }

    beforeEach(async () => {
      const safeMath = await SafeMathLib.new({from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
      await Token.link('SafeMathLib', safeMath.address)
      await Trollbox.link('SafeMathLib', safeMath.address)
      state.mgmt = accounts[0]
      state.rankManager = accounts[2]
      state.defaultHash = '0x2222222222222222222222222222222222222222222222222222222222222222'
      state.defaultENS = '0x1111111111111111111111111111111111111111111111111111111111111111'
      state.defaultRoundLength = new BN(60*60*24*10)
      state.initialBalance = new BN(1e9)
      state.defaultBonus = new BN(100)
      state.defaultMinRank = new BN(0)
      state.defaultUBI = new BN(100)
      state.defaultTickerSymbols = ['BTC', 'ETH', 'MKR', 'AAA', 'BBB', 'CCC', 'DDD', 'EEE', 'FFF', 'GGG'].map(x => web3.utils.asciiToHex(x));
      state.initialPrices = [1,2,3,4,5,6,7,8,9,10]
      state.defaultStartTime = (await getChainNow()).add(new BN(100))
      state.identity = await Identity.new(state.mgmt, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
      state.token = await Token.new(state.mgmt, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
      state.trollbox = await Trollbox.new(state.mgmt, state.rankManager, state.identity.address, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
      await state.trollbox.setToken(state.token.address, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
      await state.token.transfer(state.trollbox.address, web3.utils.toWei(state.defaultBonus), {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})

      state.oracle = await ChainLinkOracle.new(state.mgmt, state.trollbox.address, state.token.address, 1, 0, state.defaultTickerSymbols, state.initialPrices, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
      await state.oracle.setChallengeDeposit(web3.utils.toWei('10'), {from: state.mgmt, nonce: (await web3.eth.getTransactionCount(state.mgmt))})
      state.aggregators = []
      for (var i = 0; i < state.defaultTickerSymbols.length - 1; i++) {
//        console.log('deploying dummy chainlink aggregator', i)
        state.aggregators.push(await DummyChainLinkAggregator.new(i + 5, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))}))
//        console.log('adding feed', i)
        await state.oracle.addFeed(state.defaultTickerSymbols[i], state.aggregators[i].address, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
      }
      state.aggregators.push(await DummyChainLinkAggregatorETH.new(state.aggregators[0].address, state.aggregators[1].address, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))}))
      await state.oracle.addFeed(state.defaultTickerSymbols.slice(-1)[0], state.aggregators.slice(-1)[0].address, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})

      await createTournament()

    })

    afterEach(() => {});

    it('only mgmt can change mgmt key', async function () {
        const notMgmt = accounts[2];
        const contractMgmtBefore = await state.oracle.management();
        await expectRevert(state.oracle.setManagement(notMgmt, {from: notMgmt, nonce: (await web3.eth.getTransactionCount(notMgmt))}), 'Only management may call this')
        const contractMgmtAfter = await state.oracle.management();
        expect(contractMgmtAfter).to.be.a('string').that.equals(contractMgmtBefore);
        const rcpt = await state.oracle.setManagement(notMgmt, {from: contractMgmtBefore})
        expectEvent(rcpt, 'ManagementUpdated')
        const contractMgmtAfter2 = await state.oracle.management();
        expect(contractMgmtAfter2).to.be.a('string').that.equals(notMgmt);
    })

    it('only mgmt can change challenge deposit', async function () {
        const notMgmt = accounts[2];
        const newChallengeDeposit = 1;
        const challengeDepositBefore = await state.oracle.challengeDeposit();
        await expectRevert(state.oracle.setChallengeDeposit(newChallengeDeposit, {from: notMgmt, nonce: (await web3.eth.getTransactionCount(notMgmt))}), 'Only management may call this')
        const challengeDepositAfter = await state.oracle.challengeDeposit();
        expect(challengeDepositAfter).to.eq.BN(challengeDepositBefore);
        const rcpt = await state.oracle.setChallengeDeposit(newChallengeDeposit, {from: state.mgmt, nonce: (await web3.eth.getTransactionCount(state.mgmt))})
        expectEvent(rcpt, 'DepositUpdated')
        const challengeDepositAfter2 = await state.oracle.challengeDeposit();
        expect(challengeDepositAfter2).to.eq.BN(newChallengeDeposit);
    })

    it('only mgmt can change challenge period', async function () {
        const notMgmt = accounts[2];
        const newChallengePeriod = 100000;
        const challengePeriodBefore = await state.oracle.challengePeriodSeconds();
        await expectRevert(state.oracle.setChallengePeriod(newChallengePeriod, {from: notMgmt, nonce: (await web3.eth.getTransactionCount(notMgmt))}), 'Only management may call this')
        const challengePeriodAfter = await state.oracle.challengePeriodSeconds();
        expect(challengePeriodAfter).to.eq.BN(challengePeriodBefore);
        const rcpt = await state.oracle.setChallengePeriod(newChallengePeriod, {from: state.mgmt, nonce: (await web3.eth.getTransactionCount(state.mgmt))})
        expectEvent(rcpt, 'ChallengePeriodUpdated')
        const challengePeriodAfter2 = await state.oracle.challengePeriodSeconds();
        expect(challengePeriodAfter2).to.eq.BN(newChallengePeriod);
    })

    it('only mgmt can change ticker symbols', async function () {
        const notMgmt = accounts[2];
        const newTickerSymbols = ['ABC', 'DEF'].map(x => web3.utils.asciiToHex(x));
        const newPrices = [5, 6]
        const tickerSymbolsBefore = await state.oracle.getTickerSymbols();
        expect(tickerSymbolsBefore.length).to.equals(state.defaultTickerSymbols.length);
        tickerSymbolsBefore.forEach((x, i) => {
          expect(x).to.equal(web3.utils.padRight(state.defaultTickerSymbols[i], 64))
        })
        for (let i = 0; i < newTickerSymbols.length; i++) {
          const agg = await DummyChainLinkAggregator.new(i + 15)
          await state.oracle.addFeed(newTickerSymbols[i], agg.address)
        }

        await expectRevert(state.oracle.setTickerSymbols(newTickerSymbols, newPrices, {from: notMgmt, nonce: (await web3.eth.getTransactionCount(notMgmt))} ), 'Only management may call this')
        const tickerSymbolsAfter = await state.oracle.getTickerSymbols();
        expect(tickerSymbolsAfter.length).to.equals(state.defaultTickerSymbols.length);
        tickerSymbolsAfter.forEach((x, i) => {
          expect(x).to.equal(web3.utils.padRight(state.defaultTickerSymbols[i], 64))
        })

        const rcpt = await state.oracle.setTickerSymbols(newTickerSymbols, newPrices, {from: state.mgmt})
        expectEvent(rcpt, 'TickerSymbolsUpdated')
        const tickerSymbolsAfter2 = await state.oracle.getTickerSymbols();
        expect(tickerSymbolsAfter2).to.be.a('array')
        expect(tickerSymbolsAfter2.length).to.equal(newTickerSymbols.length)
        tickerSymbolsAfter2.forEach((x, i) => {
          expect(x).to.equal(web3.utils.padRight(newTickerSymbols[i], 64))
        })
    })

    it('anyone can propose winner and it generates a proposal', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        const rcpt = await state.oracle.proposeWinner({from: accounts[9]})
        const proposal = await state.oracle.proposals(1)
        const block = await web3.eth.getBlock(parseInt(rcpt.receipt.blockNumber) - 1)
        expectEvent(rcpt, 'WinnerProposed', {roundId: '1', proposalId: '1', winnerIndex: '1'})
        expect(proposal[0]).to.eq.BN('1') // id
        expect(proposal[1].sub(new BN(block.timestamp.toString()))).to.lte.BN(1) // time
        expect(proposal[2]).to.equal(false) // confirmed
        expect(proposal[3]).to.eq.BN('1') // roundId
        expect(proposal[4]).to.eq.BN('1') // winnerIndex
        expect(proposal[5]).to.eq.BN('0') // challengerWinnerIndex
        expect(proposal[6]).to.equal(ZERO_ADDRESS) // challenger
    })

    it('anyone can challenge the winner with an fvt deposit', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        const rcpt = await state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})
        const deposit = await state.oracle.challengeDeposit()
        await state.token.transfer(accounts[8], deposit, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        await state.token.approve(state.oracle.address, deposit, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        const rcpt2 = await state.oracle.challengeWinner(2, ZERO_BYTES32, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        const block = await web3.eth.getBlock(parseInt(rcpt.receipt.blockNumber) - 1)
        expectEvent(rcpt2, 'ChallengeMade', {proposalId: '1', challenger: accounts[8], claimedWinner: '2'})
        await mineOneBlock()
        const proposal = await state.oracle.proposals(1)
        expect(proposal[0]).to.eq.BN('1') // id
        expect(proposal[1].sub(new BN(block.timestamp.toString()))).to.lte.BN(1) // time
        expect(proposal[2]).to.equal(false) // confirmed
        expect(proposal[3]).to.eq.BN('1') // roundId
        expect(proposal[4]).to.eq.BN('1') // winnerIndex
        expect(proposal[5]).to.eq.BN('2') // challengerWinnerIndex
        expect(proposal[6]).to.equal(accounts[8]) // challenger
    })

    it('anyone can confirm unchallenged proposal', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        await state.oracle.proposeWinner({from: accounts[9]})
        await increaseTime((await state.oracle.challengePeriodSeconds()).toNumber() + 2)
        const rcpt = await state.oracle.confirmWinnerUnchallenged({from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        expectEvent(rcpt, 'WinnerConfirmed', {roundId: '1', proposalId: '1'})
    })

    it('only management can confirm challenged proposals', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        const rcpt = await state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})
        const block = await web3.eth.getBlock(parseInt(rcpt.receipt.blockNumber) - 1)
        const deposit = await state.oracle.challengeDeposit()
        await state.token.transfer(accounts[8], deposit, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        await state.token.approve(state.oracle.address, deposit, {from: accounts[[8]], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        const rcpt2 = await state.oracle.challengeWinner(2, ZERO_BYTES32, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        await expectRevert(state.oracle.confirmWinnerChallenged(2, [2, 3, 4, 5, 6, 7, 8, 9, 10, 11], ZERO_BYTES32,{from: accounts[2], nonce: (await web3.eth.getTransactionCount(accounts[2]))}), 'Only management may call this')

        const balanceBefore = await state.token.balanceOf(accounts[8])
        const rcpt3 = await state.oracle.confirmWinnerChallenged(2, [2, 3, 4, 5, 6, 7, 8, 9, 10, 11], ZERO_BYTES32, {from: state.mgmt, nonce: (await web3.eth.getTransactionCount(state.mgmt))})
        expectEvent(rcpt3, 'WinnerConfirmed', {roundId: '1', proposalId: '1'})
        expectEvent(rcpt3, 'ChallengerVindicated', {challenger: accounts[8], proposalId: '1'})
        await expectEvent.inTransaction(rcpt3.tx, state.trollbox, 'RoundResolved', {tournamentId: '1', roundId: '1', winningChoice: '2'})
        await expectEvent.inTransaction(rcpt3.tx, state.token, 'Transfer', {receiver: accounts[8], sender: state.oracle.address, value: deposit})

        const balanceAfter = await state.token.balanceOf(accounts[8])
        expect(balanceAfter.sub(balanceBefore)).to.eq.BN(deposit)

        const proposal = await state.oracle.proposals(1)
        expect(proposal[0]).to.eq.BN('1') // id
        expect(proposal[1].sub(new BN(block.timestamp.toString()))).to.lte.BN(1) // time
        expect(proposal[2]).to.equal(true) // confirmed
        expect(proposal[3]).to.eq.BN('1') // roundId
        expect(proposal[4]).to.eq.BN('2') // winnerIndex
        expect(proposal[5]).to.eq.BN('2') // challengerWinnerIndex
        expect(proposal[6]).to.equal(accounts[8]) // challenger

    })

    it('cannot propose winner twice', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        const rcpt = await state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})
        await expectRevert(state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))}), 'Unconfirmed proposal present')
        await expectRevert(state.oracle.proposeWinner({from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))}), 'Unconfirmed proposal present')
        await expectRevert(state.oracle.proposeWinner({from: accounts[7], nonce: (await web3.eth.getTransactionCount(accounts[7]))}), 'Unconfirmed proposal present')
    })

    it('cannot confirm proposal twice', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        await state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})
        await increaseTime((await state.oracle.challengePeriodSeconds()).toNumber() + 2)
        const rcpt = await state.oracle.confirmWinnerUnchallenged({from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        await expectRevert(state.oracle.confirmWinnerUnchallenged({from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))}), 'Already confirmed proposal')
        await expectRevert(state.oracle.confirmWinnerUnchallenged({from: accounts[7], nonce: (await web3.eth.getTransactionCount(accounts[7]))}), 'Already confirmed proposal')
    })

    it('cannot challenge proposal twice', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        await state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})
        const deposit = await state.oracle.challengeDeposit()
        await state.token.transfer(accounts[8], deposit.mul(new BN('2')), {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        await state.token.approve(state.oracle.address, deposit.mul(new BN('2')), {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        const rcpt2 = await state.oracle.challengeWinner(2, ZERO_BYTES32, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})

        await expectRevert(state.oracle.challengeWinner(2, ZERO_BYTES32, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))}), 'Proposal already challenged')

        await state.token.transfer(accounts[7], deposit, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        await state.token.approve(state.oracle.address, deposit, {from: accounts[7], nonce: (await web3.eth.getTransactionCount(accounts[7]))})
        await expectRevert(state.oracle.challengeWinner(2, ZERO_BYTES32, {from: accounts[7], nonce: (await web3.eth.getTransactionCount(accounts[7]))}), 'Proposal already challenged')
    })

    it('cannot challenge proposal after challenge period', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        await state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})
        await increaseTime((await state.oracle.challengePeriodSeconds()).toNumber() + 2)

        const deposit = await state.oracle.challengeDeposit()
        await state.token.transfer(accounts[8], deposit, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        await state.token.approve(state.oracle.address, deposit, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        await expectRevert(state.oracle.challengeWinner(2, ZERO_BYTES32, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))}), 'Challenge period has passed')

        await state.token.transfer(accounts[7], deposit, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        await state.token.approve(state.oracle.address, deposit, {from: accounts[7], nonce: (await web3.eth.getTransactionCount(accounts[7]))})
        await expectRevert(state.oracle.challengeWinner(2, ZERO_BYTES32, {from: accounts[7], nonce: (await web3.eth.getTransactionCount(accounts[7]))}), 'Challenge period has passed')
    })

    it('cannot challenge proposal with same winner', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        await state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})

        const deposit = await state.oracle.challengeDeposit()
        await state.token.transfer(accounts[8], deposit, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        await state.token.approve(state.oracle.address, deposit, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        await expectRevert(state.oracle.challengeWinner(1, ZERO_BYTES32,  {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))}), 'Must claim different winner than proposed winner')

        await state.token.transfer(accounts[7], deposit, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        await state.token.approve(state.oracle.address, deposit, {from: accounts[7], nonce: (await web3.eth.getTransactionCount(accounts[7]))})
        await expectRevert(state.oracle.challengeWinner(1, ZERO_BYTES32, {from: accounts[7], nonce: (await web3.eth.getTransactionCount(accounts[7]))}), 'Must claim different winner than proposed winner')
    })

    it('cannot confirm challenged as unchallenged', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        await state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})
        const deposit = await state.oracle.challengeDeposit()
        await state.token.transfer(accounts[8], deposit, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        await state.token.approve(state.oracle.address, deposit, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        const rcpt2 = await state.oracle.challengeWinner(2, ZERO_BYTES32, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})

        await expectRevert(state.oracle.confirmWinnerUnchallenged({from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))}), 'Proposal has been challenged')
        await expectRevert(state.oracle.confirmWinnerUnchallenged({from: accounts[7], nonce: (await web3.eth.getTransactionCount(accounts[7]))}), 'Proposal has been challenged')
    })

    it('cannot confirm unchallenged as challenged', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        await state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})
        await increaseTime((await state.oracle.challengePeriodSeconds()).toNumber() + 2)
        await expectRevert(state.oracle.confirmWinnerChallenged(2, [2, 3], ZERO_BYTES32, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))}), 'Proposal has not been challenged')
    })

    it('cannot confirm challenged with invalid winner index', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        await state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})
        const deposit = await state.oracle.challengeDeposit()
        await state.token.transfer(accounts[8], deposit, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        await state.token.approve(state.oracle.address, deposit, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        const rcpt2 = await state.oracle.challengeWinner(2, ZERO_BYTES32, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})

        await expectRevert(state.oracle.confirmWinnerChallenged(0, [2, 3, 4, 5, 6, 7, 8, 9, 10, 11], ZERO_BYTES32, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))}), 'Winner index must be positive')
        await expectRevert(state.oracle.confirmWinnerChallenged(state.defaultTickerSymbols.length + 1, [2, 3, 4], ZERO_BYTES32, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))}), 'Winner index out of range')
    })

    it('deposits get slashed for invalid challenges', async () => {
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        const rcpt = await state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})
        const block = await web3.eth.getBlock(parseInt(rcpt.receipt.blockNumber) - 1)
        const deposit = await state.oracle.challengeDeposit()
        await state.token.transfer(accounts[8], deposit, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        await state.token.approve(state.oracle.address, deposit, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        const rcpt2 = await state.oracle.challengeWinner(2, ZERO_BYTES32, {from: accounts[8], nonce: (await web3.eth.getTransactionCount(accounts[8]))})
        await expectRevert(state.oracle.confirmWinnerChallenged(2, [2, 3, 4, 5, 6, 7, 8, 9, 10, 11], ZERO_BYTES32, {from: accounts[2], nonce: (await web3.eth.getTransactionCount(accounts[2]))}), 'Only management may call this')

        const balanceBefore = await state.token.balanceOf(accounts[8])
        const rcpt3 = await state.oracle.confirmWinnerChallenged(1, [2, 3, 4, 5, 6, 7, 8, 9, 10, 11], ZERO_BYTES32, {from: state.mgmt, nonce: (await web3.eth.getTransactionCount(state.mgmt))})
        expectEvent(rcpt3, 'WinnerConfirmed', {roundId: '1', proposalId: '1'})
        expectEvent(rcpt3, 'ChallengerSlashed', {challenger: accounts[8], proposalId: '1', slashAmount: deposit})
        await expectEvent.inTransaction(rcpt3.tx, state.token, 'Transfer', {receiver: ZERO_ADDRESS, sender: state.oracle.address, value: deposit})
        await expectEvent.inTransaction(rcpt3.tx, state.token, 'TokensBurned', {burner: state.oracle.address, amount: deposit})
        await expectEvent.inTransaction(rcpt3.tx, state.trollbox, 'RoundResolved', {tournamentId: '1', roundId: '1', winningChoice: '1'})

        const balanceAfter = await state.token.balanceOf(accounts[8])
        expect(balanceAfter).to.eq.BN(balanceBefore)

        const proposal = await state.oracle.proposals(1)
        expect(proposal[0]).to.eq.BN('1') // id
        expect(proposal[1].sub(new BN(block.timestamp.toString()))).to.lte.BN(1) // time
        expect(proposal[2]).to.equal(true) // confirmed
        expect(proposal[3]).to.eq.BN('1') // roundId
        expect(proposal[4]).to.eq.BN('1') // winnerIndex
        expect(proposal[5]).to.eq.BN('2') // challengerWinnerIndex
        expect(proposal[6]).to.equal(accounts[8]) // challenger
    })

    it('allows updating token lists', async () => {
        const notMgmt = accounts[2];
        const newTickerSymbols = ['ABC', 'DEF', 'GHI', 'JKL', 'MNO'].map(x => web3.utils.asciiToHex(x));
        const newPricesBefore = [5, 6, 7, 8, 9]
        const newPricesAfter = [10, 3, 6, 7, 8]
        const tickerSymbolsBefore = await state.oracle.getTickerSymbols();
        expect(tickerSymbolsBefore.length).to.equals(state.defaultTickerSymbols.length);
        tickerSymbolsBefore.forEach((x, i) => {
          expect(x).to.equal(web3.utils.padRight(state.defaultTickerSymbols[i], 64))
        })
        for (let i = 0; i < newTickerSymbols.length; i++) {
          const agg = await DummyChainLinkAggregator.new(newPricesAfter[i], {from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})
          await state.oracle.addFeed(newTickerSymbols[i], agg.address, {from: state.mgmt, nonce: (await web3.eth.getTransactionCount(state.mgmt))})
        }

        await state.oracle.setTickerSymbols(newTickerSymbols, newPricesBefore, {from: state.mgmt, nonce: (await web3.eth.getTransactionCount(state.mgmt))})
        await increaseTime(state.defaultRoundLength.toNumber() * 3)
        const rcpt = await state.oracle.proposeWinner({from: accounts[9], nonce: (await web3.eth.getTransactionCount(accounts[9]))})
        expectEvent(rcpt, 'WinnerProposed', {roundId: '1', proposalId: '1', winnerIndex: '1'})
    })

    it('interfaces correctly with k3pr', async () => {
        const ret = await state.oracle.checkUpkeep([])
        expect(ret[0]).to.equal(false)
        expect(ret[1]).to.be.a('string').equal(ZERO_BYTES32)
        await expectRevert(state.oracle.performUpkeep(ret[1]), 'Round not ready to resolve')
        await increaseTime(state.defaultRoundLength.toNumber() * 2.5)

        const ret2 = await state.oracle.checkUpkeep([])
        expect(ret2[0]).to.equal(true)
        expect(ret2[1]).to.be.a('string').equal(ZERO_BYTES32)
        const rcpt = await state.oracle.performUpkeep(ret2[1], {nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        expectEvent(rcpt, 'WinnerProposed', {roundId: '1', proposalId: '1', winnerIndex: '1'})

        const ret3 = await state.oracle.checkUpkeep([])
        expect(ret3[0]).to.equal(false)
        expect(ret3[1]).to.be.a('string').equal(ONE_BYTES32)

        await expectRevert(state.oracle.performUpkeep(ret3[1], {nonce: (await web3.eth.getTransactionCount(accounts[0]))}), 'Challenge period has not passed')
        const challengePeriodSeconds = await state.oracle.challengePeriodSeconds()
        await increaseTime(challengePeriodSeconds.toNumber() + 1)

        const ret4 = await state.oracle.checkUpkeep([])
        expect(ret4[0]).to.equal(true)
        expect(ret4[1]).to.be.a('string').equal(ONE_BYTES32)
        const rcpt2 = await state.oracle.performUpkeep(ret4[1], {nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        expectEvent(rcpt2, 'WinnerConfirmed', {roundId: '1', proposalId: '1'})

        const ret5 = await state.oracle.checkUpkeep([])
        expect(ret5[0]).to.equal(false)
        expect(ret5[1]).to.be.a('string').equal(ZERO_BYTES32)
        await expectRevert(state.oracle.performUpkeep(ret5[1], {nonce: (await web3.eth.getTransactionCount(accounts[0]))}), 'Round not ready to resolve')
        await increaseTime(state.defaultRoundLength.toNumber())

        const ret6 = await state.oracle.checkUpkeep([])
        expect(ret6[0]).to.equal(true)
        expect(ret6[1]).to.be.a('string').equal(ZERO_BYTES32)
        const rcpt3 = await state.oracle.performUpkeep(ret6[1], {nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        expectEvent(rcpt3, 'WinnerProposed', {roundId: '2', proposalId: '2'})
        const deposit = await state.oracle.challengeDeposit()
        await state.token.approve(state.oracle.address, deposit, {from: accounts[0], nonce: (await web3.eth.getTransactionCount(accounts[0]))})
        await state.oracle.challengeWinner(2, ZERO_BYTES32, {nonce: (await web3.eth.getTransactionCount(accounts[0]))})

        const ret7 = await state.oracle.checkUpkeep([])
        expect(ret7[0]).to.equal(false)
        expect(ret7[1]).to.be.a('string').equal(ONE_BYTES32)
        await expectRevert(state.oracle.performUpkeep(ret7[1], {nonce: (await web3.eth.getTransactionCount(accounts[0]))}), 'Proposal has been challenged')
        await increaseTime(challengePeriodSeconds.toNumber())

        const ret8 = await state.oracle.checkUpkeep([])
        expect(ret8[0]).to.equal(false)
        expect(ret8[1]).to.be.a('string').equal(ONE_BYTES32)
        await expectRevert(state.oracle.performUpkeep(ret8[1], {nonce: (await web3.eth.getTransactionCount(accounts[0]))}), 'Proposal has been challenged')


    })


})