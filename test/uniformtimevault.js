'use strict';

const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545/', {
  clientConfig: { keepAlive: true, timeout: 2000*1000 },
}));
const bN = web3.utils.toBN;
const l = console.log;
const UniformTimeVault = artifacts.require('UniformTimeVault');
const Token = artifacts.require('Token');
const SafeMathLib = artifacts.require('SafeMathLib');

const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');


const chai = require('chai');
const bnChai = require('bn-chai');
chai.use(bnChai(BN));
chai.config.includeStack = true;
const expect = chai.expect;

async function assertEvents(promise, eventNames) {
    let rcpt = await promise;
    if (typeof (rcpt) === 'string') {
        rcpt = await web3.eth.getTransactionReceipt(rcpt);
    }

    assert(rcpt.logs.length === eventNames.length);
    for (let i = 0; i < rcpt.logs.length; i++) {
        assert(rcpt.logs[i].event === eventNames[i]);
    }
    return rcpt;
}

async function assertReverts(fxn, args) {
    try {
        await fxn(args);
        assert(false);
    } catch (e) {
        //
    }
}

async function increaseTime(bySeconds) {
    if (typeof(bySeconds) === 'object') {
      bySeconds = bySeconds.toNumber()
    }
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_increaseTime',
        params: [bySeconds],
        id: new Date().getTime(),
    }, (err, result) => {
      if (err) { console.error(err) }

    });
    await mineOneBlock();
}

async function mineOneBlock() {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: new Date().getTime(),
    }, () => {});
}

async function mineBlocks(numBlocks) {
    for (var i = 0; i < numBlocks; i++) {
        await mineOneBlock();
    }
}

async function getChainHeight() {
    const tip = await web3.eth.getBlock('latest')
    return new BN(tip.number);
}

async function getChainNow() {
    const tip = await web3.eth.getBlock('latest')
    return new BN(tip.timestamp);
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function almostEqual(a, b, tolerance) {
    l('almostEqual', a, b, tolerance)
    l('sub', a.sub(b))
    l('abs', a.sub(b).abs())
    expect(a.sub(b).abs()).to.be.lte.BN(tolerance);
}

contract('UniformTimeVault', function (accounts) {
    const state = {};
    const decimals = new BN('1000000000000000000')

    async function expectedWithdrawalAmount(trancheId) {
        const tranche = await state.vault.tranches(trancheId);
        const vestingPeriodEnd = tranche[5]
        let withdrawalAmount
        let chainNow = await getChainNow()
        if (chainNow.gte(vestingPeriodEnd)) {
            withdrawalAmount = tranche[3]
        } else {
            const startTime = tranche[7]
            const lastWithdrawalDate = tranche[6]
            const coinsPerSecond = tranche[2].div((tranche[5].sub(startTime)))
            withdrawalAmount = chainNow.sub(lastWithdrawalDate).mul(coinsPerSecond)
        }
        return withdrawalAmount;
    }

    async function checkWithdrawal(trancheId) {
        const trancheBefore = await state.vault.tranches(trancheId);
        const destination = trancheBefore[1];

        const currentCoinsBefore = trancheBefore[3];
        const destinationBalanceBefore = await state.token.balanceOf(destination);
        const vaultBalanceBefore = await state.token.balanceOf(state.vault.address);

        // const withdrawalAmount = await expectedWithdrawalAmount(trancheId);
        const rcpt = await state.vault.withdraw(trancheId)
        const withdrawalAmount = rcpt.logs[0].args.numTokens
        // console.log({withdrawalAmount})

        const trancheAfter = await state.vault.tranches(trancheId);

        const currentCoinsAfter = trancheAfter[3];
        const destinationBalanceAfter = await state.token.balanceOf(destination);
        const vaultBalanceAfter = await state.token.balanceOf(state.vault.address);

        expect(vaultBalanceBefore.sub(vaultBalanceAfter)).to.eq.BN(withdrawalAmount);
        expect(destinationBalanceAfter.sub(destinationBalanceBefore)).to.eq.BN(withdrawalAmount);
        expect(currentCoinsBefore.sub(currentCoinsAfter)).to.eq.BN(withdrawalAmount);

        return withdrawalAmount
    }

    async function getTokensLeft(trancheId) {
        const tranche = await state.vault.tranches(trancheId)
        return tranche[3];
    }

    beforeEach(async () => {
        const safeMath = await SafeMathLib.new()
        await Token.link('SafeMathLib', safeMath.address)
        await UniformTimeVault.link('SafeMathLib', safeMath.address)
        state.centralBank = accounts[0];
        state.initialBalance = new BN('1000000000');
        state.token = await Token.new(state.centralBank);
        state.numTranches = 10;
        state.maxLockPeriod = 100;
        state.latestVestingEnd = new BN(0);
        state.latestLockPeriodEnd = new BN(0);

        const chainNow = await getChainNow();

        state.destinations = [];
        state.tokenAllocations = [];
        state.lockPeriodEnds = [];
        state.vestingPeriodEnds = [];
        state.startTime = chainNow.sub(new BN(getRandomInt(100)));
        state.endTime = state.startTime.add(new BN(getRandomInt(1000)));

        for (var i = 0; i < state.numTranches; i++) {
            state.destinations.push(accounts[i % 10]);

            const allocation = web3.utils.toWei(new BN((getRandomInt(10) + 1) * 100))
            state.tokenAllocations.push(allocation);
        }

//        l('state', state)

        state.vault = await UniformTimeVault.new(
            state.token.address,
            state.destinations,
            state.tokenAllocations,
            state.endTime,
            state.startTime,
            {gas: 125e5}
        );
        await state.token.transfer(state.vault.address, state.initialBalance.mul(decimals), {from: state.centralBank});
    });

    afterEach(() => {});

    it('it initializes correctly', async function () {
        for (var i = 0; i < state.numTranches; i++) {
            const trancheId = i + 1;
            const tranche = await state.vault.tranches(trancheId);
            expect(tranche[1]).to.be.a('string').that.equals(state.destinations[i]);
            expect(tranche[2]).to.eq.BN(state.tokenAllocations[i]);
            expect(tranche[3]).to.eq.BN(state.tokenAllocations[i]);
            expect(tranche[4]).to.eq.BN(state.tokenAllocations[i].div(state.endTime.sub(state.startTime)));
            expect(tranche[5]).to.eq.BN(state.startTime);
        }
    });

    it('it allows token holders to withdraw 100% of their funds after vesting', async function () {
        const chainNow = await getChainNow();
        const distance = state.endTime.sub(chainNow).add(new BN(10))
        await increaseTime(distance.toNumber());
        for (var i = 0; i < state.numTranches; i++) {
            const trancheId = i + 1;
            const withdrawal = await checkWithdrawal(trancheId);
            expect(withdrawal).to.eq.BN(state.tokenAllocations[i])
        }
    });

    it('it allows you to break up withdrawals into multiple', async function () {
        const chainNow = await getChainNow();
        const sums = {}
        for (var i = 1; i <= state.numTranches; i++) {
            sums[i] = new BN(0);
        }

        while ((await getChainNow()).lte(state.endTime.add(new BN(state.numTranches)))) {
            for (var i = 0; i < state.numTranches; i++) {
                const trancheId = i + 1;
                const coinsLeft = (await getTokensLeft(trancheId)).gt(new BN(0));
                if (coinsLeft) {
                    sums[trancheId] = sums[trancheId].add(await checkWithdrawal(trancheId))
                }
                await increaseTime(1);
            }
        }

        for (var i = 0; i < state.numTranches; i++) {
            const trancheId = i+1;
            expect(sums[trancheId]).to.eq.BN(state.tokenAllocations[i]);
        }
    });

    it('it allows forwarding of destinations', async function () {
      await increaseTime(state.latestLockPeriodEnd.toNumber())
      await expectRevert(state.vault.changeDestination(1, accounts[9], {from: accounts[9]}), 'Can only change destination if you are the destination')
      await expectRevert(state.vault.changeDestination(1, accounts[9], {from: accounts[8]}), 'Can only change destination if you are the destination')
      await expectRevert(state.vault.changeDestination(1, accounts[9], {from: accounts[7]}), 'Can only change destination if you are the destination')
      await expectRevert(state.vault.changeDestination(1, accounts[9], {from: accounts[6]}), 'Can only change destination if you are the destination')
      await expectRevert(state.vault.changeDestination(1, accounts[9], {from: accounts[5]}), 'Can only change destination if you are the destination')
      expect((await state.vault.tranches(1)).destination).to.be.a('string').that.equals(accounts[0])
      await state.vault.changeDestination(1, accounts[9], {from: accounts[0]})
      expect((await state.vault.tranches(1)).destination).to.be.a('string').that.equals(accounts[9])
      await checkWithdrawal(1)
    })

});