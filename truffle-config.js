const path = require('path')
    /**
 * Use this file to configure your truffle project. It's seeded with some
 * common settings for different networks and features like migrations,
 * compilation and testing. Uncomment the ones you need or modify
 * them to suit your project as necessary.
 *
 * More information about configuration can be found at:
 *
 * truffleframework.com/docs/advanced/configuration
 *
 * To deploy via Infura you'll need a wallet provider (like truffle-hdwallet-provider)
 * to sign your transactions before they're sent to a remote public node. Infura accounts
 * are available for free at: infura.io/register.
 *
 * You'll also need a mnemonic - the twelve word phrase the wallet uses to generate
 * public/private key pairs. If you're publishing your code to GitHub make sure you load this
 * phrase from a file you've .gitignored so it doesn't accidentally become public.
 *
 */

    , HDWalletProvider = require('@truffle/hdwallet-provider')
    , testnetMnemonic = 'myth like bonus scare over problem client lizard pioneer submit female collect'
require('dotenv').config() // Store environment-specific variable from '.env' to process.env

const Web3 = require('web3');

// const HDWalletProvider = require('truffle-hdwallet-provider');
// const infuraKey = "fj4jll3k.....";
//
// const fs = require('fs');
// const mnemonic = fs.readFileSync(".secret").toString().trim();

module.exports = {
  /**
   * Networks define how you connect to your ethereum client and let you set the
   * defaults web3 uses to send transactions. If you don't specify one truffle
   * will spin up a development blockchain for you on port 9545 when you
   * run `develop` or `test`. You can ask a truffle command to use a specific
   * network from the command line, e.g
   *
   * $ truffle test --network <network-name>
   */

//  contracts_build_directory: path.resolve(__dirname, '..', 'trollbox', 'src', 'truffle', 'build', 'contracts'),
  networks: {
    // Useful for testing. The `development` name is special - truffle uses it by default
    // if it's defined here and no other network is specified at the command line.
    // You should run a client (like ganache-cli, geth or parity) in a separate terminal
    // tab if you use this network and you must also set the `host`, `port` and `network_id`
    // options below to some value.
    //
    development: {
      provider: () => new Web3.providers.HttpProvider(`http://${process.env.GANACHE ?? '127.0.0.1:8545'}/`, {
        clientConfig: { keepAlive: true, timeout: 2000*1000 },
      }),
      // provider: () => new HDWalletProvider({
      //   privateKeys: [
      //     '0000000000000000000000000000000000000000000000000000000000000001',
      //     '0000000000000000000000000000000000000000000000000000000000000002',
      //     '0000000000000000000000000000000000000000000000000000000000000003',
      //     '0000000000000000000000000000000000000000000000000000000000000004',
      //     '0000000000000000000000000000000000000000000000000000000000000005',
      //     '0000000000000000000000000000000000000000000000000000000000000006',
      //     '0000000000000000000000000000000000000000000000000000000000000007',
      //     '0000000000000000000000000000000000000000000000000000000000000008',
      //     '0000000000000000000000000000000000000000000000000000000000000009',
      //     '000000000000000000000000000000000000000000000000000000000000000a'
      //   ],
      //   providerOrUrl: 'http://127.0.0.1:8545/'
      // }),
      host: `${process.env.GANACHE ?? '127.0.0.1'}`, // Localhost (default: none)
      provider: () => new HDWalletProvider(testnetMnemonic, `ws://${process.env.GANACHE ?? '127.0.0.1:8545'}`),
      port: 8545, // Standard Ethereum port (default: none)
      network_id: '333', // Any network (default: none)
      gas: '6721975', // Ganache default
      gasPrice: '1',
      // from: '0x7E5F4552091A69125d5DfCb7b8C2659029395Bdf'
    },
    development2: {
      host: '127.0.0.1', // Localhost (default: none)
      provider: () => new HDWalletProvider(testnetMnemonic, 'ws://localhost:8546'),
      port: 8546, // Standard Ethereum port (default: none)
      network_id: '334' // Any network (default: none)
    },
    main: {
      provider: () => new HDWalletProvider(process.env.MNENOMIC, 'wss://mainnet.infura.io/ws/v3/' + process.env.INFURA_API_KEY),
      network_id: 1,
      gas: 3000000,
      gasPrice: '150000000000' // 150 Gwei
    },
    bsc: {
      provider: () => new HDWalletProvider(process.env.MNENOMIC, 'https://bsc-dataseed.binance.org/'),
      network_id: 56,
      gas: 3000000,
      gasPrice: '25000000000' // 25 GWei
    },
    matic: {
      // Use https://rpc.maticvigil.com similarly to Infura.
      provider: () => new HDWalletProvider(process.env.MNENOMIC, 'https://rpc-mainnet.maticvigil.com/v1/' + process.env.MATIC_API_KEY),
      network_id: 137,
      gas: 3000000,
      gasPrice: '1500000000' // 1.5 GWei
    },
    xdai: {
      provider: () => new HDWalletProvider(process.env.MNENOMIC, 'https://rpc.xdaichain.com/'),
      network_id: 100,
      gas: 3000000,
      gasPrice: '1000000000' // 1 GWei
    },
    bridge: {
      provider: () => new HDWalletProvider(process.env.MNENOMIC2, 'wss://mainnet.infura.io/ws/v3/' + process.env.INFURA_API_KEY),
      network_id: 1,
      port: 8545,
    },
    // Another network with more advanced options...
    // advanced: {
    // port: 8777,             // Custom port
    // network_id: 1342,       // Custom network
    // gas: 8500000,           // Gas sent with each transaction (default: ~6700000)
    // gasPrice: 20000000000,  // 20 gwei (in wei) (default: 100 gwei)
    // from: <address>,        // Account to send txs from (default: accounts[0])
    // websockets: true        // Enable EventEmitter interface for web3 (default: false)
    // },

    // Useful for deploying to a public network.
    // NB: It's important to wrap the provider as a function.
    ropsten: {
//      provider: () => new HDWalletProvider(testnetMnemonic, `https://ropsten.infura.io/v3/${process.env.INFURA_API_KEY}`),
      provider: () => new HDWalletProvider(process.env.MNENOMIC, `https://ropsten.infura.io/v3/${process.env.INFURA_API_KEY}`),
      network_id: 3, // Ropsten's id
      gas: 5500000, // Ropsten has a lower block limit than mainnet
      confirmations: 2, // # of confs to wait between deployments. (default: 0)
      timeoutBlocks: 200, // # of blocks before a deployment times out  (minimum/default: 50)
      skipDryRun: true // Skip dry run before migrations? (default: false for public nets )
    }
    // Useful for private networks
    // private: {
    // provider: () => new HDWalletProvider(mnemonic, `https://network.io`),
    // network_id: 2111,   // This network is yours, in the cloud.
    // production: true    // Treats this network as if it was a public net. (default: false)
    // }
  },

  // Set default mocha options here, use special reporters etc.
    mocha: {
      reporter: 'eth-gas-reporter',
      reporterOptions: {
        currency: 'USD',
        gasPrice: 60,
        url:'http://localhost:8545'
      },
    },
  // Configure your compilers
  compilers: {
    solc: {
       version: "0.7.4",    // Fetch exact version from solc-bin (default: truffle's version)
      // docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
      // settings: {          // See the solidity docs for advice about optimization and evmVersion
        optimizer: {
          enabled: true,
          runs: 1000
        },
      //  evmVersion: "byzantium"
      // }
    }
  }
}
